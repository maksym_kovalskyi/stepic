from selenium import webdriver


try:
    browser = webdriver.Chrome()
    browser.implicitly_wait = 5
    link = " http://suninjuly.github.io/cats.html"
    browser.get(link)

    browser.find_element_by_id("button")

finally:
    # закрываем браузер после всех манипуляций
    browser.quit()