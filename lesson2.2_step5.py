import math
from selenium import webdriver
from selenium.webdriver.common.by import By


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/execute_script.html"
    browser = webdriver.Chrome()
    browser.get(link)

    val1 = "#input_value"
    val2 = "input#answer"
    val3 = "#robotCheckbox"
    val4 = "#robotsRule"
    val5 = "button"
    x = browser.find_element_by_css_selector(val1).text
    y = calc(x)
    browser.find_element_by_css_selector(val2).send_keys(y)
    browser.find_element_by_css_selector(val3).click()
    browser.execute_script("window.scrollBy(0, 100);")
    browser.find_element_by_css_selector(val4).click()
    button = browser.find_element_by_css_selector(val5)
    button.click()


finally:
    # закрываем браузер после всех манипуляций
    browser.quit()