import os

from selenium import webdriver
import time


link = "http://suninjuly.github.io/file_input.html"

try:
    current_dir = os.path.abspath(os.path.dirname(__file__))  # получаем путь к директории текущего исполняемого файла
    file_path = os.path.join(current_dir, 'file.txt')  # добавляем к этому пути имя файла

    browser = webdriver.Chrome()
    browser.get(link)
    value1 = "input[name = 'firstname']"
    value2 = "input[name = 'lastname']"
    value3 = "input[name = 'email']"
    value4 = "input#file"

    input1 = browser.find_element_by_css_selector(value1)
    input1.send_keys("Maks")
    input2 = browser.find_element_by_css_selector(value2)
    input2.send_keys("Kovalksyi")
    input3 = browser.find_element_by_css_selector(value3)
    input3.send_keys("test@mail.com")
    input4 = browser.find_element_by_css_selector(value4)
    input4.send_keys(file_path)

    button = browser.find_element_by_xpath(".//button[text()='Submit']")
    button.click()

    print(current_dir)
    print(file_path)


finally:
    # успеваем скопировать код за 30 секунд
    time.sleep(30)
    # закрываем браузер после всех манипуляций
    browser.quit()

# не забываем оставить пустую строку в конце файла