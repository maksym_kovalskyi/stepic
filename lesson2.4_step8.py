import math

from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    browser = webdriver.Chrome()
    wait = WebDriverWait(browser, 15)
    link = "http://suninjuly.github.io/explicit_wait2.html"

    browser.get(link)
    x_value_locator = "#input_value"
    input_locator = "input#answer"
    submit_locator = "#solve"
    price_locator = "#price"
    book_locator = "#book"

    vaa = wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, price_locator), '$100'))
    browser.find_element_by_css_selector("#book").click()
    x_value = browser.find_element_by_css_selector(x_value_locator).text
    calculated_result = calc(x_value)

    browser.find_element_by_css_selector(input_locator).send_keys(calculated_result)
    browser.find_element_by_css_selector(submit_locator).click()


finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
