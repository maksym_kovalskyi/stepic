from selenium import webdriver


try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/registration1.html"
    browser.get(link)

    # Ваш код, который заполняет обязательные поля
    xpath_first_name = ".//input[@placeholder='Input your first name']"
    xpath_last_name = ".//input[@placeholder='Input your last name']"
    xpath_email = ".//input[@placeholder='Input your email']"
    xpath_phone = ".//input[@placeholder='Input your phone:']"
    xpath_address = ".//input[@placeholder='Input your address:']"

    input1 = browser.find_element_by_xpath(xpath_first_name)
    input1.send_keys("Maks")
    input2 = browser.find_element_by_xpath(xpath_last_name)
    input2.send_keys("Kovalskyi")
    input3 = browser.find_element_by_xpath(xpath_email)
    input3.send_keys("test@mail.com")
    input4 = browser.find_element_by_xpath(xpath_phone)
    input4.send_keys("77777777777")
    input5 = browser.find_element_by_xpath(xpath_address)
    input5.send_keys("Ukraine")

    # Отправляем заполненную форму
    button = browser.find_element_by_css_selector("button[type = 'submit']")
    button.click()

    # Проверяем, что смогли зарегистрироваться
    # ждем загрузки страницы
    # time.sleep(1)

    # находим элемент, содержащий текст
    welcome_text_elt = browser.find_element_by_css_selector("div.container h1")
    # записываем в переменную welcome_text текст из элемента welcome_text_elt
    welcome_text = welcome_text_elt.text
    # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
    assert "Congratulations! You have successfully registered!" == welcome_text

finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    #time.sleep(10)
    # закрываем браузер после всех манипуляций
    browser.quit()

