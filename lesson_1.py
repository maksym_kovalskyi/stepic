from selenium import webdriver
import math
import time

from selenium.webdriver.common.by import By

link = "http://suninjuly.github.io/find_xpath_form"
value = "input"

try:
    browser = webdriver.Chrome(executable_path='.//chromedriver.exe')
    # browser.get("http://suninjuly.github.io/huge_form.html")
    # elements = browser.find_elements_by_css_selector("input")
    # for element in elements:
    #    element.send_keys("Мой ответ")
    #
    # button = browser.find_element_by_css_selector("button.btn")
    # button.click()

    browser.get(link)
    value1 = "input[name = 'first_name']"
    value2 = "input[name = 'last_name']"
    value3 = "input[name = 'firstname']"
    value4 = "input#country"

    input1 = browser.find_element_by_css_selector(value1)
    input1.send_keys("Ivan")
    input2 = browser.find_element_by_css_selector(value2)
    input2.send_keys("Petrov")
    input3 = browser.find_element_by_css_selector(value3)
    input3.send_keys("Smolensk")
    input4 = browser.find_element_by_css_selector(value4)
    input4.send_keys("Smolensk")

    button = browser.find_element_by_xpath(".//button[text()='Submit']")
    button.click()



finally:
    # успеваем скопировать код за 30 секунд
    time.sleep(30)
    # закрываем браузер после всех манипуляций
    browser.quit()

# не забываем оставить пустую строку в конце файла