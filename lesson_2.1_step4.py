import math
from selenium import webdriver
from selenium.webdriver.common.by import By


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/get_attribute.html"
    browser = webdriver.Chrome()
    browser.get(link)

    val1 = "img"
    val2 = "input#answer"
    val3 = "#robotCheckbox"
    val4 = "#robotsRule"
    val5 = "button"
    x_element = browser.find_element_by_css_selector(val1)
    x = x_element.get_attribute("valuex")
    y = calc(x)

    browser.find_element_by_css_selector(val2).send_keys(y)
    browser.find_element_by_css_selector(val3).click()
    browser.find_element_by_css_selector(val4).click()
    browser.find_element_by_css_selector(val5).click()


finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
