from selenium import webdriver
from selenium.webdriver.support.select import Select


def calc(x, y):
    a = int(x)
    b = int(y)
    return a + b


try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/selects1.html"
    browser.get(link)

    x = browser.find_element_by_css_selector("#num1").text
    y = browser.find_element_by_css_selector("#num2").text
    c = str(calc(x, y))
    select = Select(browser.find_element_by_css_selector("select#dropdown"))
    select.select_by_value(c)
    browser.find_element_by_css_selector("button").click()


finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
