import math
import time

from selenium import webdriver


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    link = "http://suninjuly.github.io/alert_accept.html"
    browser = webdriver.Chrome()
    browser.get(link)

    val1 = "#input_value"
    val2 = "input#answer"
    val3 = "button"
    browser.find_element_by_css_selector(val3).click()
    confirm = browser.switch_to.alert
    confirm.accept()
    time.sleep(2)
    x = browser.find_element_by_css_selector(val1).text
    y = calc(x)

    browser.find_element_by_css_selector(val2).send_keys(y)
    browser.find_element_by_css_selector(val3).click()


finally:
    # закрываем браузер после всех манипуляций
    browser.quit()