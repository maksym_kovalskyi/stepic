import math

from selenium import webdriver
from selenium.webdriver.support.select import Select


def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


try:
    browser = webdriver.Chrome()
    link = " http://suninjuly.github.io/redirect_accept.html"
    browser.get(link)

    browser.find_element_by_css_selector("button").click()

    val1 = "#input_value"
    val2 = "input#answer"
    val3 = "button"

    tabs = browser.window_handles
    browser.switch_to.window(tabs[1])

    x = browser.find_element_by_css_selector(val1).text
    y = calc(x)

    browser.find_element_by_css_selector(val2).send_keys(y)
    browser.find_element_by_css_selector(val3).click()
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()